//! APIs for interacting with Compute Express Link (CXL) devices.

pub mod lsa;
pub mod query;
pub mod send;
mod sysfs;

use crate::sysfs::*;
use serde::{Deserialize, Serialize};
use std::{fmt, fs, io, path::Path, path::PathBuf};

/// Magic IOCTL number from the kernel.
pub const CXL_IOC_MAGIC: u8 = 0xce;

/// A CXL memory device.
#[derive(Debug, Serialize, Deserialize)]
pub struct Memdev {
    #[serde(skip_serializing)]
    sysfs_path: PathBuf,
    #[serde(skip_serializing)]
    chardev_path: PathBuf,

    name: String,
    fw_vers: String,
    payload_max: u64,
    pmem: u64,
    ram: u64,
}

impl fmt::Display for Memdev {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl Memdev {
    /// Returns a memory device for the given path
    pub fn new(path: &str) -> Option<Self> {
        let p = Path::new(path);
        if p.is_dir() {
            let name = &p
                .file_name()
                .map(|name| name.to_string_lossy().into_owned())
                .unwrap_or_else(|| "".into());
            let sysfs_path = &PathBuf::from(path);
            Some(Memdev {
                sysfs_path: sysfs_path.to_path_buf(),
                name: name.to_string(),
                chardev_path: PathBuf::from("/dev/cxl/").join(name),
                fw_vers: get_sysfs_node(path, "firmware_version").ok()?,
                payload_max: get_sysfs_node_u64(path, "payload_max").ok()?,
                pmem: get_sysfs_node_hex(path, "pmem/size").ok()?,
                ram: get_sysfs_node_hex(path, "ram/size").ok()?,
            })
        } else {
            None
        }
    }

    /// Get the name of the memory device (ie. mem1)
    pub fn get_name(&self) -> &str {
        &self.name
    }
}

/// Returns CXL memory devices found in sysfs.
///
/// # Arguments
///
/// * `path` - Optional path to start the search
///
/// # Examples
///
/// ```
/// #[cfg(feature = "cxlhost")] {
///     let files = cxl_rs::get_sysfs_devices(None);
///     for devs in files {
///         // do something like create a memdev
///     }
/// }
/// ```
pub fn get_sysfs_devices(path: Option<&Path>) -> io::Result<Vec<(String, PathBuf)>> {
    let default = format!("{}/{}", CXL_SYSFS_BASE, "devices/");
    let sysfs_dir = path.unwrap_or_else(|| Path::new(&default));
    let mut result = Vec::<(String, PathBuf)>::new();

    let files = fs::read_dir(sysfs_dir).unwrap();
    files
        .filter_map(Result::ok)
        .filter_map(|d| {
            d.path().file_name().and_then(|f| {
                if f.to_string_lossy().starts_with("mem") {
                    Some(d)
                } else {
                    None
                }
            })
        })
        .for_each(|f| {
            result.push((
                f.file_name().to_str().unwrap().to_string(),
                f.path().canonicalize().unwrap(),
            ))
        });

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn no_devices() {
        assert_eq!(0, get_sysfs_devices(Some(Path::new("/tmp"))).unwrap().len());
    }

    #[test]
    #[cfg(feature = "cxlhost")]
    fn cxl_devices() {
        assert_ne!(0, get_sysfs_devices(None).unwrap().len());
    }

    #[test]
    #[cfg(feature = "cxlhost")]
    fn mem0() {
        assert!(Memdev::new("/sys/bus/cxl/devices/mem0").is_some());
    }
}
