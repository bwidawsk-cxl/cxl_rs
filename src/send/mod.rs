//! Implement send commands
//!
//! CXL Linux driver interface supports a SEND command which is capable of - duh - sending
//! commands. The list of supported commands can be obtained by [query](super::query)

pub mod send_bindings;

use super::CXL_IOC_MAGIC;
use crate::Memdev;
use nix::ioctl_readwrite;
use send_bindings::cxl_send_command;
use serde::{Deserialize, Serialize, Serializer};
use std::{
    ffi::CStr,
    fs::OpenOptions,
    io,
    mem::MaybeUninit,
    os::unix::{fs::OpenOptionsExt, io::AsRawFd},
};

const CXL_IOC_NR_SEND: u8 = 2;

ioctl_readwrite!(
    cxl_send_cmd,
    CXL_IOC_MAGIC,
    CXL_IOC_NR_SEND,
    cxl_send_command
);

fn fw_str<S>(x: &[::std::os::raw::c_char; 16usize], s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(unsafe { CStr::from_ptr(x.as_ptr()).to_string_lossy().into_owned() }.as_str())
}

/// Structure representing the output payload of the IDENTIFY command.
#[repr(C, packed)]
#[derive(Serialize, Deserialize)]
pub struct IdentifyPayload {
    #[serde(serialize_with = "fw_str")]
    pub fw_revision: [::std::os::raw::c_char; 16usize],
    pub total_capacity: u64,
    pub volatile_capacity: u64,
    pub persistent_capacity: u64,
    pub partition_alignment: u64,
    pub info_event_log_size: u16,
    pub warn_event_log_size: u16,
    pub fail_event_log_size: u16,
    pub fatal_event_log_size: u16,
    pub lsa_size: u32,
    pub poison_list_max_records: [u8; 3],
    pub inject_poison_limit: u16,
    pub poison_caps: u8,
    pub telemetry: u8,
}

#[test]
fn identify_test_layout() {
    assert_eq!(
        ::std::mem::size_of::<IdentifyPayload>(),
        67usize,
        concat!("Size of: ", stringify!(cxl_send_command))
    );
}

/* https://github.com/rust-embedded/rust-spidev/blob/master/src/spidevioctl.rs#L16 */
fn from_nix_error(err: ::nix::Error) -> io::Error {
    io::Error::from_raw_os_error(err.as_errno().unwrap_or(nix::errno::Errno::UnknownErrno) as i32)
}

/* https://github.com/rust-embedded/rust-spidev/blob/master/src/spidevioctl.rs#L23 */
fn from_nix_result<T>(res: ::nix::Result<T>) -> io::Result<T> {
    match res {
        Ok(r) => Ok(r),
        Err(err) => Err(from_nix_error(err)),
    }
}

impl Memdev {
    #[cfg(target_endian = "little")]
    fn send_cmd(&self, mut cmd: cxl_send_command) -> Result<i32, std::io::Error> {
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .custom_flags(libc::O_NONBLOCK)
            .open(self.chardev_path.as_path())?;

        from_nix_result(unsafe { cxl_send_cmd(file.as_raw_fd(), &mut cmd) })
    }
    #[cfg(target_endian = "big")]
    fn send_cmd(&self, mut cmd: cxl_send_command) -> Result<i32, std::io::Error> {
        panic!("Big endian platforms aren't supported yet.");
    }

    /// Returns the output from the IDENTIFY command.
    ///
    /// The identify command may or may not actually be run as a result of this. The function may
    /// used cached information.
    ///
    /// # Examples
    ///
    /// ```
    /// #[cfg(feature = "cxlhost")] {
    /// # use std::io;
    /// fn main() -> io::Result<()> { #[allow(non_snake_case)]
    ///     let memdev = cxl_rs::Memdev::new("/sys/bus/cxl/devices/mem0").expect("Bad memory device");
    ///     let id = memdev.identify()?;
    ///     println!("Found device with {} capacity", id.total_capacity);
    ///     Ok(())
    /// }
    /// }
    /// ```
    pub fn identify(&self) -> Result<IdentifyPayload, std::io::Error> {
        let mut out: [MaybeUninit<u8>; ::std::mem::size_of::<IdentifyPayload>()] =
            unsafe { MaybeUninit::uninit().assume_init() };

        let cmd: cxl_send_command = cxl_send_command {
            id: send_bindings::CXL_MEM_COMMAND_ID_IDENTIFY,
            flags: 0,
            __bindgen_anon_1: send_bindings::cxl_send_command__bindgen_ty_1 { rsvd: 0 },
            retval: 0,
            in_: send_bindings::cxl_send_command__bindgen_ty_2 {
                size: 0,
                rsvd: 0,
                payload: 0,
            },
            out: send_bindings::cxl_send_command__bindgen_ty_3 {
                size: 0x43,
                rsvd: 0,
                payload: out.as_mut_ptr() as u64,
            },
        };
        self.send_cmd(cmd)?;
        Ok(unsafe { std::ptr::read(out.as_ptr() as *const _) })
    }
}
