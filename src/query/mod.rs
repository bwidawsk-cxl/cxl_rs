//! Implements query commands
//!
//! The CXL Linux driver interface supports a QUERY command which is capable of obtaining the list
//! of commands supported by the driver and enabled by the hardware. These commands can be
//! dispatched via [send](super::send)
pub mod query_bindings;

/// Utilize the CXL QUERY IOCTL interface to obtain information about the memory device.
///
/// Parts of this file are generated with rust-bindgen
use super::CXL_IOC_MAGIC;
use crate::Memdev;
use libc::c_int;
use nix::request_code_read;
use query_bindings::{cxl_command_info, cxl_mem_query_commands};
use std::{
    fs::OpenOptions,
    io,
    mem::MaybeUninit,
    os::unix::{fs::OpenOptionsExt, io::AsRawFd},
    ptr::addr_of_mut,
};

// Max commands supported by the UAPI's Query command. The UAPI itself theoretically has a max of
// 2^32, so it's possible this can become too small over time. This const is artificial and is
// only used because there seems to be no good other way to handle the flex array member in the
// cxl_mem_query_commands structure.
const CXL_MAX_COMMANDS: usize = 100;

// These defines come from include/uapi/linux/cxl_mem.h
const CXL_IOC_NR_QUERY: u8 = 1;

/// Command names.
///
/// Command names are actually part of the UAPI.
///
/// # TODO
///
/// - have bindgen doesn't seem to capture it.
pub const CXL_MEM_COMMAND_NAMES: &[&str] = &[
    "Invalid Command",
    "Identify Command",
    "Raw device command",
    "Get Supported Logs",
    "Get FW Info",
    "Get Partition Information",
    "Get Label Storage Area",
    "Get Health Info",
    "Get Log",
    "Set Partition Information",
    "Set Label Storage Area",
    "Get Alert Configuration",
    "Set Alert Configuration",
    "Get Shutdown State",
    "Set Shutdown State",
    "Get Poison List",
    "Inject Poison",
    "Clear Poison",
    "Get Scan Media Capabilities",
    "Scan Media",
    "Get Scan Media Results",
];

impl Memdev {
    unsafe fn __cxl_mem_query_cmds(
        fd: c_int,
        data: *mut cxl_mem_query_commands<[cxl_command_info; CXL_MAX_COMMANDS]>,
    ) -> Result<(), std::io::Error> {
        let res = libc::ioctl(
            fd,
            request_code_read!(CXL_IOC_MAGIC, CXL_IOC_NR_QUERY, 8_u32),
            data,
        );

        if res != 0 {
            return Err(io::Error::from_raw_os_error(res));
        }

        Ok(())
    }

    fn commands(
        &self,
        num_commands: usize,
    ) -> Result<Vec<(cxl_command_info, &str)>, std::io::Error> {
        assert!(
            num_commands < CXL_MAX_COMMANDS,
            "Need a larger array size. Please rebuild"
        );
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .custom_flags(libc::O_NONBLOCK)
            .open(self.chardev_path.as_path())?;

        let mut qcmd = {
            let mut uninit: MaybeUninit<
                cxl_mem_query_commands<[cxl_command_info; CXL_MAX_COMMANDS]>,
            > = MaybeUninit::uninit();
            let ptr = uninit.as_mut_ptr();

            unsafe {
                addr_of_mut!((*ptr).n_commands).write(num_commands as u32);
                addr_of_mut!((*ptr).rsvd).write(0_u32);
                addr_of_mut!((*ptr).commands).write(
                    [cxl_command_info {
                        id: 0,
                        flags: 0,
                        size_in: 0,
                        size_out: 0,
                    }; CXL_MAX_COMMANDS],
                );
                uninit.assume_init()
            }
        };

        unsafe {
            Memdev::__cxl_mem_query_cmds(file.as_raw_fd(), &mut qcmd)?;
        }

        let commands: Vec<cxl_command_info> = qcmd.commands[..qcmd.n_commands as usize].to_vec();
        Ok(commands
            .into_iter()
            .map(|info| (info, CXL_MEM_COMMAND_NAMES[info.id as usize]))
            .collect::<Vec<_>>())
    }

    fn get_num_commands(&self) -> Result<usize, std::io::Error> {
        Ok(self.commands(0)?.len())
    }

    /// Returns the list of (commands, name) supported by the device
    ///
    /// # Examples
    ///
    /// ```
    /// #[cfg(feature = "cxlhost")] {
    /// # use std::io;
    /// # use cxl_rs::query::query_bindings::cxl_command_info;
    /// fn main() -> io::Result<()> { #[allow(non_snake_case)]
    ///     let memdev = cxl_rs::Memdev::new("/sys/bus/cxl/devices/mem0").expect("Bad memory device");
    ///     let cmds = memdev.get_commands()?;
    ///     println!("There are {} commands supported", cmds.len());
    ///     Ok(())
    /// }
    /// }
    /// ```
    pub fn get_commands(&self) -> io::Result<Vec<(cxl_command_info, &str)>> {
        let n = self.get_num_commands()?;
        self.commands(n)
    }

    /// Returns whether or not a command is enabled for a device
    ///
    /// # Examples
    ///
    /// ```
    /// #[cfg(feature = "cxlhost")] {
    /// # use std::io;
    /// # use cxl_rs::send::send_bindings::CXL_MEM_COMMAND_ID_IDENTIFY;
    /// fn main() -> io::Result<(bool)> { #[allow(non_snake_case)]
    ///     let memdev = cxl_rs::Memdev::new("/sys/bus/cxl/devices/mem0").expect("Bad memory device");
    ///     let id_enabled: bool = memdev.is_command_enabled(CXL_MEM_COMMAND_ID_IDENTIFY)?;
    ///     Ok(id_enabled)
    /// }
    /// }
    /// ```
    pub fn is_command_enabled(&self, id: u32) -> Result<bool, std::io::Error> {
        let cmds = self.get_commands()?;
        Ok(cmds.iter().any(|&cmd| cmd.0.id == id))
    }
}
