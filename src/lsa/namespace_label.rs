use crate::label_as_bytes;
use crate::lsa::{generate_checksum, to_hex, CommonLabelProperties};
use crate::{lsa, set_checksum, verify_checksum};
use serde::{Serialize, Serializer};
use std::{
    io::{Error, ErrorKind},
    slice, str,
};
use uuid::Uuid;

fn rsvd<S>(x: &[u8; 0x56], s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if x.iter().all(|&y| y == 0) {
        s.serialize_str("RSVD (0)")
    } else {
        s.serialize_str("RSVD (non-zero)")
    }
}

fn name_str<S>(x: &[u8; 0x40], s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(str::from_utf8(x).unwrap_or("Noname"))
}
pub const NAMESPACE_UUID: Uuid = Uuid::from_bytes([
    0x68, 0xbb, 0x2c, 0x0a, 0x5a, 0x77, 0x49, 0x37, 0x9f, 0x85, 0x3c, 0xaf, 0x41, 0xa0, 0xf9, 0x3c,
]);

#[repr(packed)]
#[derive(Clone, Copy, Serialize)]
#[allow(non_snake_case)]
pub struct NamespaceLabel {
    #[serde(serialize_with = "lsa::to_uuid")]
    Type: [u8; 16],
    #[serde(serialize_with = "lsa::to_uuid")]
    UUID: [u8; 16],

    #[serde(serialize_with = "name_str")]
    Name: [u8; 0x40],
    Flags: u32,
    NRange: u16,
    Position: u16,
    DPA: u64,
    RawSize: u64,
    Slot: u32,
    Alignment: u32,
    #[serde(serialize_with = "lsa::to_uuid")]
    RegionUUID: [u8; 16],
    #[serde(serialize_with = "lsa::to_uuid")]
    AddressAbstractionUUID: [u8; 16],
    LBASize: u16,
    #[serde(serialize_with = "rsvd")]
    Reserved: [u8; 0x56],
    #[serde(serialize_with = "to_hex")]
    Checksum: u64,
}

#[test]
fn namespace_test_layout() {
    assert_eq!(
        ::std::mem::size_of::<NamespaceLabel>(),
        256usize,
        concat!("Size of: ", stringify!(NamespaceLabel))
    );
}

impl Default for NamespaceLabel {
    fn default() -> NamespaceLabel {
        NamespaceLabel {
            Type: *NAMESPACE_UUID.as_bytes(),
            UUID: [0; 16],

            Name: *b"Unitialized Namespace                                          \0",
            Flags: 0,
            NRange: 0, // Block namespace
            Position: 0,
            DPA: 0,
            RawSize: 0,
            Slot: 0,
            Alignment: 0,
            RegionUUID: [0; 16],
            AddressAbstractionUUID: [0; 16],
            LBASize: 0,
            Reserved: [0; 0x56],
            Checksum: 0,
        }
    }
}

impl NamespaceLabel {
    pub fn new() -> NamespaceLabel {
        let mut namespace = NamespaceLabel {
            ..Default::default()
        };
        namespace.UUID = *Uuid::new_v4().as_bytes();
        namespace
    }

    pub fn name(mut self, name: &str) -> NamespaceLabel {
        self.Name.fill(0);
        self.Name[0..name.len()].copy_from_slice(name.as_bytes());
        self
    }

    pub fn size(mut self, size: usize) -> NamespaceLabel {
        self.RawSize = size as u64;
        self
    }

    pub fn region_uuid(mut self, uuid: Uuid) -> NamespaceLabel {
        self.RegionUUID = *uuid.as_bytes();
        self
    }
}

impl CommonLabelProperties for NamespaceLabel {
    fn update_checksum(&mut self) {
        set_checksum!(self);
    }
    fn verify(&self) -> Result<(), std::io::Error> {
        verify_checksum!(self)
    }
    fn as_bytes(&self) -> &[u8] {
        label_as_bytes!(self)
    }
    fn from_bytes(buf: &[u8]) -> Self {
        assert!(buf.len() == 256);
        unsafe { std::ptr::read(buf.as_ptr() as *const _) }
    }
    fn set_slot(&mut self, slot: usize) {
        self.Slot = slot as u32
    }
}
