//! Label Index Block
//!
//! The Label Index Block(s) are a pair of data structures stored on the device as part of the LSA
//! that allow software to deduce properties and organization of the underlying media.
use crate::{
    label_as_bytes,
    lsa::{generate_checksum, to_hex, CommonLabelProperties},
    set_checksum, verify_checksum,
};
use serde::{
    ser::{SerializeSeq, Serializer},
    Serialize,
};
use std::{
    io::{Error, ErrorKind},
    slice, str,
};

fn sig_str<S>(x: &[u8; 16], s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(&str::from_utf8(x).unwrap()[..16])
}

fn free<S>(x: &[u8; 184], s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let num_bits = x.iter().fold(0, |acc, e| acc + e.count_zeros());
    let mut seq = s.serialize_seq(Some(num_bits as usize))?;

    for (i, e) in x.iter().enumerate() {
        if e.count_ones() == 8 {
            // Only free slots
            continue;
        }

        let mut bits = Vec::with_capacity(8);
        for n in 0..8 {
            if (e >> n) & 1 == 0 {
                bits.push(i * 8 + n);
            }
        }

        seq.serialize_element(&bits)?;
    }

    seq.end()
}

#[repr(packed)]
#[derive(Clone, Copy, Serialize)]
#[allow(non_snake_case)]
pub struct LabelIndexBlock {
    #[serde(serialize_with = "sig_str")]
    Sig: [u8; 16],

    Flags: [u8; 3],
    LabelSize: u8,
    Seq: u32,
    MyOff: u64,
    MySize: u64,
    OtherOff: u64,
    LabelOff: u64,
    NSlot: u32,
    Major: u16,
    Minor: u16,
    #[serde(serialize_with = "to_hex")]
    Checksum: u64,

    #[serde(rename(serialize = "In use"), serialize_with = "free")]
    Free: [u8; 184], // Max 1472 slots
}

#[test]
fn lbi_test_layout() {
    assert_eq!(
        ::std::mem::size_of::<LabelIndexBlock>(),
        256usize,
        concat!("Size of: ", stringify!(LabelIndexBlock))
    );
}

impl LabelIndexBlock {
    /// Returns a new LabelIndexBlock
    ///
    /// The label index block is fully initialized but is invalid as there is no sequence number,
    /// nor active slots (and therefore no checksum is generated.
    pub fn new(which: usize, max_slots: usize) -> LabelIndexBlock {
        if max_slots > 1472 {
            todo!();
        }
        assert!(which < 2);

        let mut size = 72 + max_slots / 8;
        // align to 256B
        size = (size + 255) & !255usize;
        if size != 256 {
            todo!();
        }

        let offsets: (usize, usize) = match which {
            0 => (0, size),
            1 => (size, 0),
            _ => {
                panic!();
            }
        };

        LabelIndexBlock {
            Sig: *b"NAMESPACE_INDEX\0",
            Flags: [0; 3],
            LabelSize: 1,
            Seq: 0, // Invalid
            MyOff: offsets.0 as u64,
            MySize: size as u64,
            OtherOff: offsets.1 as u64,
            LabelOff: (size * 2) as u64,
            NSlot: max_slots as u32,
            Major: 2,
            Minor: 1,
            Checksum: 0, // Incorrect
            Free: [0xff; 184],
        }
    }

    fn set_free(&mut self, slot: usize, value: u8) {
        assert!(value < 2);
        let which_byte = slot / 8;
        let which_bit = slot % 8;
        let mask = !(1 << which_bit);
        let mut val = self.Free[which_byte];
        val &= mask;
        val |= value << which_bit;

        self.Free[which_byte] = val;
    }

    pub fn is_block0(&self) -> bool {
        self.MyOff == 0
    }

    /// Set a slot as used
    pub fn retain_slot(&mut self, slot: usize) {
        if slot > self.NSlot as usize {
            panic!("Invalid slot retained");
        }

        self.set_free(slot, 0);
    }

    fn is_sig_valid(&self) -> bool {
        self.Sig == *b"NAMESPACE_INDEX\0"
    }

    pub fn get_sequence(&self) -> u32 {
        self.Seq
    }

    pub fn set_sequence(&mut self, seq: u32) {
        self.Seq = seq;
    }

    fn other_offset_valid(&self) -> bool {
        //self.OtherOff == XX
        true
    }

    // CXL 2.0 spec states:
    //    When reading Label Index Blocks, software shall only consider index blocks valid when
    //    their Sig, MyOff, OtherOff, and Checksum fields are correct. In addition, any blocks
    //    with Seq set to zero are discarded as invalid.
    pub fn valid(&self) -> Result<(), &str> {
        if !self.is_sig_valid() {
            return Err("Invalid signature");
        }

        if self.is_block0() {
            if self.MyOff != 0 {
                return Err("Invalid offset");
            }
            if !self.other_offset_valid() {
                return Err("Invalid other offset");
            }
        } else {
            if self.OtherOff != 0 {
                return Err("Invalid other offset");
            }
            if self.MyOff % 256 != 0 {
                return Err("Invalid offset");
            }
            // TODO: check offset based on NSlot
        }

        if self.Seq > 3 || self.Seq == 0 {
            return Err("Sequence number invalid");
        }

        let used_slots = self.Free.iter().fold(0, |acc, e| acc + e.count_zeros());
        if used_slots > self.NSlot {
            return Err("More used slots than NSlot");
        }

        // FIXME: 256
        for i in self.NSlot..256 {
            if self.Free[i as usize] != 0xff {
                return Err("Invalid free bit cleared (above NSlot)");
            }
        }

        match verify_checksum!(self) {
            Ok(_) => Ok(()),
            Err(_) => Err("Failed checksum"),
        }
    }
}

impl CommonLabelProperties for LabelIndexBlock {
    fn update_checksum(&mut self) {
        set_checksum!(self);
    }
    fn verify(&self) -> Result<(), std::io::Error> {
        verify_checksum!(self)
    }
    fn as_bytes(&self) -> &[u8] {
        label_as_bytes!(self)
    }

    fn from_bytes(buf: &[u8]) -> Self {
        assert!(buf.len() == 256);
        unsafe { std::ptr::read(buf.as_ptr() as *const _) }
    }
    fn set_slot(&mut self, _slot: usize) {
        panic!("Invalid label command");
    }
}
