use crate::{
    label_as_bytes, lsa,
    lsa::{generate_checksum, to_hex, CommonLabelProperties},
    set_checksum, verify_checksum,
};
use serde::{Serialize, Serializer};
use std::{
    io::{Error, ErrorKind},
    slice,
};
use uuid::Uuid;

fn rsvd<S>(x: &[u8; 0xac], s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if x.iter().all(|&y| y == 0) {
        s.serialize_str("RSVD (0)")
    } else {
        s.serialize_str("RSVD (non-zero)")
    }
}

pub const REGION_UUID: Uuid = Uuid::from_bytes([
    0x52, 0x9d, 0x7c, 0x61, 0xda, 0x07, 0x47, 0xc4, 0xa9, 0x3f, 0xec, 0xdf, 0x2c, 0x06, 0xf4, 0x44,
]);

#[repr(packed)]
#[derive(Serialize, Copy, Clone, Debug)]
#[allow(non_snake_case)]
pub struct RegionLabel {
    #[serde(serialize_with = "lsa::to_uuid")]
    Type: [u8; 16],
    #[serde(serialize_with = "lsa::to_uuid")]
    UUID: [u8; 16],

    Flags: u32,
    NLabel: u16,
    Position: u16,
    DPA: u64,
    RawSize: u64,
    HPA: u64,
    Slot: u32,
    InterleaveGranularity: u32,
    Alignment: u32,
    #[serde(serialize_with = "rsvd")]
    Reserved: [u8; 0xac],
    #[serde(serialize_with = "to_hex")]
    Checksum: u64,
}

#[test]
fn region_label_test_layout() {
    assert_eq!(
        ::std::mem::size_of::<RegionLabel>(),
        256usize,
        concat!("Size of: ", stringify!(RegionLabel))
    );
}

impl Default for RegionLabel {
    fn default() -> RegionLabel {
        RegionLabel {
            Type: *REGION_UUID.as_bytes(),
            UUID: [0; 16],

            Flags: 0,
            NLabel: 1,
            Position: 0,
            DPA: 0,
            RawSize: 0,
            HPA: 0,
            Slot: 0,
            InterleaveGranularity: 0,
            Alignment: 0,
            Reserved: [0xe; 0xac],
            Checksum: 0,
        }
    }
}

#[test]
fn region_test_layout() {
    assert_eq!(
        ::std::mem::size_of::<RegionLabel>(),
        256usize,
        concat!("Size of: ", stringify!(RegionLabel))
    );
}

impl RegionLabel {
    pub fn new() -> RegionLabel {
        let mut region = RegionLabel {
            ..Default::default()
        };
        region.UUID = *Uuid::new_v4().as_bytes();
        region
    }

    // Builder pattern for the individual fields
    pub fn size(mut self, size: usize) -> RegionLabel {
        self.RawSize = size as u64;
        self
    }

    pub fn uuid(mut self, uuid: Uuid) -> RegionLabel {
        self.UUID = *uuid.as_bytes();
        self
    }

    pub fn interleave(mut self, interleave: (usize, usize)) -> RegionLabel {
        self.NLabel = interleave.0 as u16;
        self.Position = interleave.1 as u16;
        self
    }
}

impl CommonLabelProperties for RegionLabel {
    fn update_checksum(&mut self) {
        set_checksum!(self);
    }
    fn verify(&self) -> Result<(), std::io::Error> {
        assert!(self.UUID == *REGION_UUID.as_bytes());
        verify_checksum!(self)
    }
    fn as_bytes(&self) -> &[u8] {
        label_as_bytes!(self)
    }

    fn from_bytes(buf: &[u8]) -> Self {
        assert!(buf.len() == 256);
        unsafe { std::ptr::read(buf.as_ptr() as *const _) }
    }
    fn set_slot(&mut self, slot: usize) {
        self.Slot = slot as u32
    }
}
