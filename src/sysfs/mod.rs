use std::{fs, path::Path};

pub const CXL_SYSFS_BASE: &str = "/sys/bus/cxl";

/// Returns the contents of a sysfs node
///
/// # Arguments
///
///  * `path` - Top level directory of a CXL sysfs node.
///  * `node` - Specific node without the directory @path.
#[cfg(target_family = "unix")]
pub fn get_sysfs_node(path: &str, node: &str) -> Result<String, std::io::Error> {
    let f = Path::new(path).join(node);
    Ok(fs::read_to_string(f)?.trim_end().to_string())
}

/// Returns the parsed hex value from a sysfs node.
///
/// # Arguments
///
///  * `path` - Top level directory of a CXL sysfs node.
///  * `node` - Specific node without the directory @path.
#[cfg(target_family = "unix")]
pub fn get_sysfs_node_hex(path: &str, node: &str) -> Result<u64, std::io::Error> {
    Ok(u64::from_str_radix(&get_sysfs_node(path, node)?[2..], 16).expect("Couldn't parse"))
}

/// Returns a u64 value from a sysfs node.
///
/// # Arguments
///
///  * `path` - Top level directory of a CXL sysfs node.
///  * `node` - Specific node without the directory @path.
#[cfg(target_family = "unix")]
pub fn get_sysfs_node_u64(path: &str, node: &str) -> Result<u64, std::io::Error> {
    Ok(get_sysfs_node(path, node)?.parse().expect("Couldn't parse"))
}

#[cfg(test)]
#[cfg(target_family = "unix")]
mod tests {
    use super::*;
    use std::io;

    #[test]
    fn bad_node() {
        let result = get_sysfs_node("\\badbad", "STILL BAD").map_err(|e| e.kind());
        let expected = Err(io::ErrorKind::NotFound);
        assert_eq!(expected, result);
    }

    // This isn't super safe, but all recent kernels use 0x20f...
    #[test]
    fn hex_node() {
        assert_eq!(
            get_sysfs_node_hex("/sys/", "kernel/boot_params/version").unwrap(),
            527
        );
    }

    #[test]
    fn u64_node() {
        assert!(get_sysfs_node_u64("/sys/", "kernel/rcu_normal").is_ok());
    }
}
