# Rust bindings for CXL Linux interfaces

cxl_rs attempts to grant the low level functionality which is needed to write a
utility that would have functionality to the CXL utility that's part of the
[ndctl](https://github.com/pmem/ndctl) utility.

The library has 3 main responsibilities:
1. Parsing file system entities representing CXL devices, like sysfs.
2. Interfacing with the character device IOCTL interface
3. Managing memory devices, ie provisioning a device, and interleave set
   management.

## Status
Very limited functionality is currently supported by library. The following
areas need work:
1. Better tests. Tests are difficult to write because the host system usually
   will not have CXL devices.
2. More idiomatic rust. Lots of code was written as a Rust novice and could use
   some conversion.
3. Implement missing memory device commands.
4. Handle provisioning (currently Linux driver has no support for this)
5. Handle logging/debugging
6. Make human readable size serializers
7. Run rust-bindgen on UAPI as part of build

## Contributing

Contributions are welcome. Currently the following areas are in need of work:
